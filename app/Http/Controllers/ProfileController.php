<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;

class ProfileController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | ProfileController
    |--------------------------------------------------------------------------
    |
    | This controller handles the account details of the user. 
    |
    */

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');
    }

    /**
     * Display User's Profile page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profile', array('user' => Auth::user()));
    }

    /**
     * Update User's Email in the Storage.
     *
     * @return Saves User Email in Db.
     */
    public function updateEmail(Request $request)
    {
        $updateEmail = $request->all();
        extract($updateEmail);

        $user = Auth::user();
        $user->email = $email;
        $user->save();
    }
}
