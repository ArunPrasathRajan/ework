<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;

use App\Product;
use App\Sale;
use Carbon\Carbon;

use DB;

class ProductSalesController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | ProfileController
    |--------------------------------------------------------------------------
    |
    | This controller handles the order and sales of products. 
    |
    */

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::select('name')
        ->get();

        $orders = 0;

        $orders=DB::table('sales')
        ->join('products','sales.product_name','=','products.name')
        ->select('sales.product_name','sales.product_quantity','products.price','sales.date')
        ->get();

        return view('productSales', array('user' => Auth::user(), 'products' => $products, 'orders' => $orders));
    }

    /**
     * creating a new order and saving it in the db.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $newOrder = $request->all();
        // dd($newOrder);

        extract($newOrder);
        $order = new Sale;

        $order->date = date('Y-m-d', strtotime($date));
        $order->product_name = $product;
        $order->product_quantity = (int)$productQuantity;
        $order->save();

        return redirect('/product-sales');
    }

    
}
