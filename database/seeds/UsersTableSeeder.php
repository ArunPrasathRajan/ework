<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        DB::table('users')->insert([
            [
                'name' => 'Arun',
	        	'email' => 'arun@arun.com',
                'password' => bcrypt('arunraj'),
                'phone' => 7577702994
        	],
        	[
        		'name'	=>	'Ashok',
        		'email'	=>	'ashok@ashok.com',
                'password'  =>  bcrypt('ashokumar'),
                'phone' => 1234567890
        	]
        ]);
    }
}
