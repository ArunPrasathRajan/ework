<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->truncate();

        DB::table('products')->insert([
        	[
        		'name' => 'A',
        		'price' => 10
        	],

        	[
        		'name' => 'B',
        		'price' => 20
        	],

        	[
        		'name' => 'C',
        		'price' => 30
        	],

        	[
        		'name' => 'D',
        		'price' => 40
        	],
        	
        	[
        		'name' => 'E',
        		'price' => 50
        	],
        ]);
    }
}
