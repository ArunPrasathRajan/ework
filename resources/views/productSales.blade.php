@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <button class="btn btn-success pull-right addNew" >Create Order<span class="caret"></span></button>

        <form class="form-inline" role="form" method="POST" action="{{ url('/saveOrder') }}">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="date">Date:</label>
              <input type="date" class="form-control" name="date" placeholder="dd/mm/yyyy">
            </div>
            <div class="form-group">
                <label for="productName">Product:</label>
                <select class="form-control" id="product" name="product" >
                @foreach($products as $product)
                    <option>{{$product->name}}</option>
                @endforeach()
                </select>
            </div>
            <div class="form-group">
              <label for="productQuantity">Quantity:</label>
              <input type="number" class="form-control" id="productQuantity" name="productQuantity">
          </div>

          <button type="submit" class="btn btn-default">Submit</button>
        </form>
</div>

<div class="row">
    <table class="table table-striped text-center" id="aru">
        <thead>
            <tr>
              <th class="text-center">Date</th>
              <th class="text-center">Product Name</th>
              <th class="text-center">Product Quantity</th>
              <th class="text-center">Price</th>
            </tr>
        </thead>
        <tbody>
        @if(!empty($orders))
            @foreach($orders as $order)
            <tr>
                <td >{{ Carbon\Carbon::parse($order->date)->format('d-m-Y') }}</td>
                <td >{{$order->product_name}}</td>
                <td >{{$order->product_quantity}}</td>
                <td ><?php echo ($order->product_quantity * $order->price) ?></td>
             </tr>   
            @endforeach
         @endif
         </tbody>
    </table>  
</div>
</div>
@endsection